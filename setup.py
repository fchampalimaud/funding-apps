from setuptools import setup, find_packages

setup(
    name="funding-apps",
    version=0.1,
    description="""""",
    author=["Ricardo Ribeiro"],
    author_email=["ricardojvr@gmail.com"],
    packages=find_packages(),
    include_package_data=True,
    package_data={
        "funding_opportunities_apps": [
            "templates/*.html",
            "templates/timeline/*.html",
            "static/css/*.css",
            "static/img/*",
        ]
    },
)
