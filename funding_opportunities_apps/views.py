from datetime import datetime, timedelta
import json
import openpyxl
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.utils import timezone
from django.utils.timezone import make_aware
from funding_opportunities_models.models import (FundingOpportunity,
                                                 OpportunitySubject)
from funding_opportunities_models.resources.funding_opportunity import \
    FundingOpportunityResource
from funding_opportunities_models.resources.opportunity_topic import \
    OpportunityTopicResource
from openpyxl.writer.excel import save_virtual_workbook

try:
	from django.utils import simplejson
except:
	import json as simplejson


@login_required
def export_funding_opportunities_data(request):
	# create resource and export it
	ds_topics = OpportunityTopicResource().export()
	# prepare query with related relationships so that the export will be faster by using the data in cache (select_related won't work on m2m relationships)
	fo = FundingOpportunity.objects.select_related(	'currency',
													'paymentfrequency',
													'subject',
													'financingAgency',
													'fundingtype').prefetch_related('topics').filter(fundingopportunity_end__gt=make_aware(datetime(2015, 12, 31))).all()

	ds_funding_opportunities = FundingOpportunityResource().export(fo)

	# we need to use openpyxl directly so we can overwrite the dates
	wb = openpyxl.Workbook()
	wb.iso_dates = True
	ws = wb.active

	ws.title = "Funding Opportunities"
	ws.append(ds_funding_opportunities.headers + ['Email'])
	for cell in ws[1:1]:
		cell.font = openpyxl.styles.Font(bold=True)

	ws.freeze_panes = "A2"

	idx_deadline = ds_funding_opportunities.headers.index('Deadline (Lisbon time)')
	idx_loideadline = ds_funding_opportunities.headers.index('LOI deadline')
	idx_fullproposal = ds_funding_opportunities.headers.index('Full proposal deadline')
	idx_created = ds_funding_opportunities.headers.index('Created')
	idx_updated = ds_funding_opportunities.headers.index('Updated')
	date_format = '%Y-%m-%d %H:%M:%S'

	for row in ds_funding_opportunities:
		# this conversion is required because for some reason the ds_funding_opportunities returned always has dates as their string representation
		deadline = (datetime.strptime(row[idx_deadline], date_format),) if row[idx_deadline] else (None,)
		loideadline = (datetime.strptime(row[idx_loideadline], date_format),) if row[idx_loideadline] else (None,)
		fullproposal = (datetime.strptime(row[idx_fullproposal], date_format),) if row[idx_fullproposal] else (None,)
		created = (datetime.strptime(row[idx_created], date_format),) if row[idx_created] else (None,)
		updated = (datetime.strptime(row[idx_updated], date_format),) if row[idx_updated] else (None,)
		# NOTE: this is dependant on the order of the Resource. The last item is dissemination_dates
		changed_row = row[:idx_deadline] + deadline + row[idx_deadline+1:idx_loideadline] + loideadline + fullproposal + created + updated
		# insert duplicated lines for each dissemination date
		if row[-1]:
			dd = json.loads(row[-1])
			for d in dd:
				new_row = changed_row + ((datetime.strptime(d['opportunitydissemination_date'], '%Y-%m-%d'), d['opportunitydissemination_email'] ))
				ws.append(new_row)
		else:
			ws.append(changed_row)

	for col in ['E', 'R', 'S', 'T', 'U', 'V']:			# columns with dates
		ws.column_dimensions[col].width = 20	

	ws.auto_filter.ref = ws.dimensions

	ws_topics = wb.create_sheet("Topics")
	ws_topics.append(ds_topics.headers)
	for cell in ws_topics[1:1]:
		cell.font = openpyxl.styles.Font(bold=True)

	for row in ds_topics:
		ws_topics.append(row)
	
	ws_topics.freeze_panes = "A2"

	# prepare response as xlsx
	response = HttpResponse( content=save_virtual_workbook(wb),
							 content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
	response['Content-Disposition'] = f'attachment; filename="{timezone.now().strftime("%Y-%m-%d %H:%M:%S")} funding opportunities export data.xlsx"'

	return response


def funding_opportunities_timeline(request):
	template = 'timeline/funding-opportunities.html'
	subjects = OpportunitySubject.objects.all().order_by('opportunitysubject_name')
	return render_to_response( template, {'subjects': subjects} )

def funding_opportunities_json(request):
	template = 'timeline/funding-opportunities.html'

	today = timezone.now()
	funds = FundingOpportunity.objects.filter(fundingopportunity_end__gte=today).order_by('fundingopportunity_end')
	user = request.user
	#groups = user.groups.all()

	res = []
	for fund in funds:
		classes = []
		#if fund.groups.filter(group=groups).exists(): classes.append('mine')
		classes.append( 'subject'+str(fund.subject.pk) )
		
		two_weeks_ago = timezone.now() + timedelta(days=-14) 
		if fund.fundingopportunity_createdon>=two_weeks_ago: classes.append('new')

		obj = { 
			'classes':		' '.join(classes),
			"pk": 			fund.pk,
			"title": 		fund.fundingopportunity_name,
			"date":  		fund.fundingopportunity_end.isoformat(),
			"display_date":	fund.fundingopportunity_end.strftime("%d. %B"),
			"enddate":  	fund.fundingopportunity_end.strftime("%d. %B %Y"),
    		"grantor":		fund.financingAgency.grantor_name,
    		"subject":		fund.subject.opportunitysubject_name,
			"body":			fund.fundingopportunity_brifdesc,
			"read_more":	fund.fundingopportunity_link,
			"topics":		', '.join([str(x) for x in fund.topics.all()]),
		}
		#if fund.financingAgency.grantor_icon: obj['photo_url'] = "/media/{0}".format( fund.financingAgency.grantor_icon )
		 
		res.append(obj)

	return HttpResponse(simplejson.dumps(res), content_type='application/json')

