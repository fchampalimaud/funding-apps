from django.conf.urls import url
from django.urls import path
from funding_opportunities_apps.views import funding_opportunities_timeline, funding_opportunities_json, export_funding_opportunities_data

urlpatterns =  [
	url(r'^fundingopportunities-timeline/$', 		funding_opportunities_timeline),
	path('export_funding_opportunities_data/', export_funding_opportunities_data, name="export_funding_opportunities_data"),
	url(r'^fundingopportunities-timeline/ajax/$', 	funding_opportunities_json),
]