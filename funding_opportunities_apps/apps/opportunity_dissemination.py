from confapp import conf
from django.conf import settings
from pyforms_web.organizers import segment
from pyforms_web.widgets.django.modeladmin import ModelAdminWidget
from pyforms_web.widgets.django.modelform import ModelFormWidget
from funding_opportunities_models.models import OpportunityDissemination


class OpportunityDisseminationForm(ModelFormWidget):
    MODEL = OpportunityDissemination

    AUTHORIZED_GROUPS = [settings.PERMISSION_EDIT_FUNDING, 'superuser']

    FIELDSETS = [
        "h4:Dissemination dates",
        segment("opportunitydissemination_date", "opportunitydissemination_email",),
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class OpportunityDisseminationApp(ModelAdminWidget):

    MODEL = OpportunityDissemination

    AUTHORIZED_GROUPS = [settings.PERMISSION_EDIT_FUNDING, 'superuser']

    ########################################################
    #### ORQUESTRA CONFIGURATION ###########################
    ########################################################
    LAYOUT_POSITION = conf.ORQUESTRA_NEW_TAB
    ########################################################

    EDITFORM_CLASS = OpportunityDisseminationForm

    LIST_DISPLAY = [
        "opportunitydissemination_date",
        "opportunitydissemination_email",
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_queryset(self, request, queryset):
        return queryset.filter(fundingopportunity__fundingopportunity_id=int(self.parent_pk))

    def has_add_permissions(self):
        return False